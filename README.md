# my-s3-backup
Simple backup CLI tool to sent file into AWS S3 storage.

## Properties
All properties are required. Properties should be defined manually at `config.properties` and placed in same directory as backup jar itself:

```
access_key = AWS access key
secret_key = AWS secret key
region_name = AWS region name
bucket = AWS bucket_name
file = full path to file (in property files for Windows should be double slash)
new_name = file new name (optional, default current filename)
root_dir = backup root folder name (optional, default mybackup)
```

## CLI options
Use `-h` or `--help` to print available arguments.

## Build JAR

```
gradlew shadowJar
```

## Scheduling
Tool doesn't have build-in scheduler feature and should be triggered by third party tools (e.g. cron job).
