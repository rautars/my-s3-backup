package org.rustylab.s3backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.rustylab.s3backup.aws.AmazonS3Factory;

/**
 * Loads configuration from given file. By default loads from
 * {@code config.properties} located in same directory where jar itself.
 */
public class ConfigLoader {

    private static final String CONFIG_FILE_NAME = "config.properties";
    private static final String AWS_ACCESS_KEY = "access_key";
    private static final String AWS_SECRET_KEY = "secret_key";
    private static final String AWS_REGION_NAME = "region_name";
    private static final String AWS_BUCKET_NAME = "bucket";
    private static final String FILE_TO_BACKUP = "file";
    private static final String NEW_FILE_NAME = "new_name";
    private static final String BACKUP_ROOT_FOLDER_NAME = "root_dir";

    private Properties prop = new Properties();

    public ConfigLoader(String configPath) {
        if (configPath != null) {
            loadProperties(configPath);
        } else {
            loadProperties(getDefaultConfigPath());
        }
    }
    
    private void loadProperties(String configPath) {
        try (InputStream input = new FileInputStream(configPath)) {
            prop.load(input);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static String getDefaultConfigPath() {
        URL url = AmazonS3Factory.class.getProtectionDomain().getCodeSource().getLocation();
        try {
            File jarFile = new File(url.toURI().getPath());
            return jarFile.getParent() + File.separator + CONFIG_FILE_NAME;
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }
    
    public String getAccessKey() {
        return prop.getProperty(AWS_ACCESS_KEY);
    }

    public String getSecretKey() {
        return prop.getProperty(AWS_SECRET_KEY);
    }

    public String getRegionName() {
        return prop.getProperty(AWS_REGION_NAME);
    }

    public String getFile() {
        return prop.getProperty(FILE_TO_BACKUP);
    }

    public String getBucket() {
        return prop.getProperty(AWS_BUCKET_NAME);
    }

    public String getRootDir() {
        return prop.getProperty(BACKUP_ROOT_FOLDER_NAME);
    }

    public String getNewName() {
        return prop.getProperty(NEW_FILE_NAME);
    }
}
