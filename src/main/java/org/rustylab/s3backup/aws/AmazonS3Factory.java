package org.rustylab.s3backup.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * Responsible for preparing {@link AmazonS3} instance.
 */
public class AmazonS3Factory {

    private static AWSCredentials createCredentials(String accessKey, String secretKey) {
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    private static Regions getRegion(String regionName) {
        return Regions.fromName(regionName);
    }
    
    public static AmazonS3 create(String regionName, String accessKey, String secretKey) {
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(createCredentials(accessKey, secretKey)))
                .withRegion(getRegion(regionName))
                .build();
        return s3client;
    }
}
