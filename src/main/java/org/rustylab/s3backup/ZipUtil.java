package org.rustylab.s3backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

    private static final Logger logger = Logger.getLogger(ZipUtil.class.getName());

    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS");
    private static String tmpDir = System.getProperty("java.io.tmpdir");

    /**
     * Compress file in zip format.
     * 
     * @param sourceFile full path to file which needs to be added to zip
     * @param prefix zip file name prefix
     */
    public static String zipFile(String sourceFile, String prefix) {
        File fileToZip = new File(sourceFile);
        LocalDateTime currentDate = LocalDateTime.now();
        String fileName = tmpDir + File.separator + prefix + "_" + currentDate.format(dateFormatter) + ".zip";
        try (
                FileOutputStream fos = new FileOutputStream(fileName);
                ZipOutputStream zipOut = new ZipOutputStream(fos);
                FileInputStream fis = new FileInputStream(fileToZip.toURI().getPath());
            ) {

            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);

            final byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return fileName;
    }

}
